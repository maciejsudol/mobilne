package com.example.sudol.firstapp;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sudol.utils.Album;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    Button previous;
    Button raw;
    Button next;

    TextView artist;
    TextView album;
    TextView genre;
    TextView year;
    TextView tracks;

    List<Album> albums;
    Integer albumIterator;

    URL endpoint;
    HttpsURLConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.albums = new ArrayList<>();
        this.albumIterator = 0;

        this.prepareViews();

        try {
            this.endpoint = new URL("https://isebi.net/albums.php");
        } catch (MalformedURLException exception) {
            exception.printStackTrace();
        }

        this.readJSON();
    }

    private void prepareViews() {
        this.previous = findViewById(R.id.previous);
        this.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });

        this.raw = findViewById(R.id.raw);
        this.raw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                raw();
            }
        });

        this.next = findViewById(R.id.next);
        this.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        this.artist = findViewById(R.id.artist);
        this.album = findViewById(R.id.album);
        this.genre = findViewById(R.id.genre);
        this.year = findViewById(R.id.year);
        this.tracks = findViewById(R.id.tracks);
    }

    private void previous() {
        if ((this.albumIterator > 0)) {
            this.albumIterator--;
        }

        this.updateView(this.albums.get(this.albumIterator));
    }

    private void raw() {
        try {
            Uri uri = Uri.parse(this.endpoint.toURI().toString());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } catch (URISyntaxException exception) {
            exception.printStackTrace();
        }
    }

    private void next() {
        if (this.albumIterator < (this.albums.size() - 1)) {
            this.albumIterator++;
        }

        this.updateView(this.albums.get(this.albumIterator));
    }

    private void updateView(Album album) {
        this.artist.setText(album.getArtist());
        this.album.setText(album.getAlbum());
        this.genre.setText(album.getGenre());
        this.year.setText(album.getYear().toString());
        this.tracks.setText(album.getTracks().toString());
    }

    private void readJSON() {
        AsyncTask.execute(new Runnable() {
            InputStream responseBody;
            InputStreamReader responseBodyReader;
            JsonReader jsonReader;

            @Override
            public void run() {
                try {
                    connection = (HttpsURLConnection) endpoint.openConnection();
                    connection.setRequestProperty("User-Agent", "my-rest-app-v0.1");

                    if (connection.getResponseCode() == 200) {
                        this.responseBody = connection.getInputStream();
                        this.responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                        this.jsonReader = new JsonReader(responseBodyReader);

                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            Album album = new Album();

                            jsonReader.beginObject();
                            while (jsonReader.hasNext()) {
                                String jsonName = jsonReader.nextName();
                                String jsonValue = jsonReader.nextString();

                                switch (jsonName.toLowerCase()) {
                                    case "artist":
                                        album.setArtist(jsonValue);
                                        break;
                                    case "album":
                                        album.setAlbum(jsonValue);
                                        break;
                                    case "genre":
                                        album.setGenre(jsonValue);
                                        break;
                                    case "year":
                                        album.setYear(Integer.parseInt(jsonValue));
                                        break;
                                    case "tracks":
                                        album.setTracks(Integer.parseInt(jsonValue));
                                        break;
                                    default:
                                        throw new IOException("Unexpected JSON key found...");
                                }
                            }
                            jsonReader.endObject();
                            albums.add(album);
                        }
                    } else {
                        throw new IOException("JSON read attempt returned: " + connection.getResponseCode() + " code...");
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                } finally {
                    try {
                        jsonReader.close();
                        connection.disconnect();
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }

                    albumIterator = 0;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateView(albums.get(0));
                        }
                    });
                }
            }
        });
    }
}
